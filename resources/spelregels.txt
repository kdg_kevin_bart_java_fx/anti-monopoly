Monopoly

In monopoly bewegen de spelers hun pionnen door een aantal steden. Deze steden bestaan uit 2 of 3 straten. Als een speler aan de beurt is, gooit hij twee dobbelstenen. Het aantal ogen geeft aan hoeveel plaatsen hun pion verplaatst wordt. Stopt de pion op een vak waarvan het eigendomsrecht nog niet gekocht is, dan mag je dit eigendom kopen. De prijs staat op het spelbord. Stopt jouw pion op een vak waarvan een andere speler het eigendomsrecht heeft, dan moet er huur betaald worden. Het bedrag van de huur staat op het eigendomsbewijs.
Door het bouwen van huizen en hotels stijgt de huurprijs van een eigendom.
Telkens als de spelers op het vak start terechtkomen of dit vak passeren, ontvangen ze 100€.
Spelers kunnen eventueel eigendommen verkopen aan andere spelers of aan de bank om aan extra geld te komen.

Monopolisten

Monopolisten kunnen alleen bouwen in steden waarvan ze minstens 2 straten bezitten. In dit geval ontvangen ze ook dubbele huur als die straten onbebouwd blijven.
Monopolisten mogen tot 3 huizen bouwen op een straat. In plaats van een vierde huis te bouwen, geven ze die drie huizen terug aan de bankier en kopen ze een hotel.
Monopolisten kunnen naar het gevang gestuurd worden. Om uit het gevang te geraken moeten ze 50€ betalen of dubbel gooien. Monopolisten in het gevang ontvangen geen huur.
Monopolisten die op het monopolistenvak terechtkomen, nemen een kanskaart van de monopolistenstapel.
Monopolisten die inkomstenbelasting moeten betalen verliezen 200€ of 20% van hun geld + 10% van de grondprijs van hun straten en bedrijven die niet aan de bank verkocht zijn (= hypotheek) + 10 % van de waarde van hun huizen en hotels.
Monopolisten die een transportbedrijf bezitten verdubbelen hun tarieven telkens als ze er een ander transportbedrijf bijkopen.
Monopolisten die de gasmaatschappij of het elektriciteitsbedrijf bezitten, ontvangen 4x het aantal gegooide ogen aan huurprijs. Bezitten ze beide bedrijven, dan ontvangen ze 10x het aantal gegooide ogen.
Monopolisten die op het vak anti-monopoly stichting terechtkomen verliezen 160€

Concurrenten

Concurrenten kunnen direct bouwen, ook in steden waar ze maar 1 straat bezitten. Concurrenten ontvangen nooit dubbele huur.
Concurrenten mogen tot 4 huizen bouwen op een straat. In plaats van een vijfde huis te bouwen, geven ze die vier huizen terug aan de bankier en kopen ze een hotel.
Concurrenten kunnen naar de prijzenoorlog gestuurd worden. Om uit de prijzenoorlog te geraken moeten ze 50€ betalen of dubbel gooien. Terwijl Concurrenten in de prijzenoorlog zitten, blijven ze huur ontvangen.
Concurrenten die op het concurrentenvak terechtkomen, nemen een kanskaart van de concurrentenstapel.
Concurrenten die inkomstenbelasting moeten betalen verliezen 200€ of 10% van hun geld + 10% van de grondprijs van hun straten en bedrijven die niet aan de bank verkocht zijn (= hypotheek) + 10 % van de waarde van hun huizen en hotels.
Concurrenten die een transportbedrijf bezitten verdubbelen hun tarieven niet als ze er een ander transportbedrijf bijkopen.
Concurrenten die de gasmaatschappij en/of het elektriciteitsbedrijf bezitten, ontvangen 4x het aantal gegooide ogen aan huurprijs.
Concurrenten die op het vak anti-monopoly stichting terechtkomen, gooien één dobbelsteen. Een 1 levert 25€ op, een 2 levert 50€ op. Elke andere worp levert niks op.

De winnaar

Zijn alle monopolisten failliet, dan wint de rijkste concurrent. Zijn alle concurrenten failliet dan wint de rijkste monopolist. Het geld en de inkomsten die verdiend kunnen worden bepalen het vermogen van een speler.