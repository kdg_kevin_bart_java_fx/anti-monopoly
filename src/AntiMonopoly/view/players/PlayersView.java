package AntiMonopoly.view.players;

import AntiMonopoly.model.PlayerRole;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.awt.*;

public class PlayersView extends VBox {
    private Label label;
    private TextField speler1;
    private ComboBox<String> rolSpeler1;
    private TextField speler2;
    private ComboBox<String> rolSpeler2;
    private TextField speler3;
    private ComboBox<String> rolSpeler3;
    private TextField speler4;
    private ComboBox<String> rolSpeler4;
    private Button button;

    public PlayersView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.label = new Label("Geef de namen van de spelers en kies hun rol (min. 2 spelers, max. 4 spelers)");
        this.speler1 = new TextField();
        this.speler2 = new TextField();
        this.speler3 = new TextField();
        this.speler4 = new TextField();
        this.button = new Button("Bevestig Spelers");
    }

    private void layoutNodes() {
        //Background
        Image image = new Image("/images/MonopolyMan.png");
        BackgroundSize backgroundSize =
                new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false,
                        false, false, false);
        Background background =
                new Background(new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize));
        this.setBackground(background);


        //button
        this.button.setMaxWidth(Double.MAX_VALUE);
        this.button.setMaxHeight(100);
        VBox.setVgrow(button, Priority.ALWAYS);
        //label
        this.label.setStyle("-fx-background-color: white;");
        //textboxes + comboboxes
        this.rolSpeler1 = new ComboBox<>();
        this.rolSpeler2 = new ComboBox<>();
        this.rolSpeler3 = new ComboBox<>();
        this.rolSpeler4 = new ComboBox<>();
        ObservableList<String> namen = FXCollections.observableArrayList(PlayerRole.MONOPOLIST.name(), PlayerRole.CONCURRENT.name());
        this.rolSpeler1.setItems(namen);
        this.rolSpeler2.setItems(namen);
        this.rolSpeler3.setItems(namen);
        this.rolSpeler4.setItems(namen);
        this.rolSpeler1.setVisible(false);
        this.rolSpeler2.setVisible(false);
        this.rolSpeler3.setVisible(false);
        this.rolSpeler4.setVisible(false);
        this.rolSpeler1.setPromptText("Rol speler 1");
        this.rolSpeler2.setPromptText("Rol speler 2");
        this.rolSpeler3.setPromptText("Rol speler 3");
        this.rolSpeler4.setPromptText("Rol speler 4");
        //this.rolSpeler1.getSelectionModel().select(0);
        this.speler1.setPromptText("Naam speler 1");
        this.speler2.setPromptText("Naam speler 2");
        this.speler3.setPromptText("Naam speler 3");
        this.speler3.setEditable(false);
        this.speler3.setStyle("-fx-background-color: grey;");
        this.speler4.setPromptText("Naam speler 4");
        this.speler4.setEditable(false);
        this.speler4.setStyle("-fx-background-color: grey;");
        this.setSpacing(20);
        this.setPadding(new Insets(20));
        this.getChildren().addAll(label,speler1,rolSpeler1, speler2, rolSpeler2, speler3, rolSpeler3, speler4, rolSpeler4, button);
        this.setAlignment(Pos.TOP_LEFT);
        rolSpeler4.requestFocus();

    }

    Label getLabel() {
        return label;
    }

    Button getButton() {
        return button;
    }

    TextField getSpeler1() {
        return speler1;
    }

    ComboBox<String> getRolSpeler1() {
        return rolSpeler1;
    }

    TextField getSpeler2() {
        return speler2;
    }

    ComboBox<String> getRolSpeler2() {
        return rolSpeler2;
    }

    TextField getSpeler3() {
        return speler3;
    }

    ComboBox<String> getRolSpeler3() {
        return rolSpeler3;
    }

    TextField getSpeler4() {
        return speler4;
    }

    ComboBox<String> getRolSpeler4() {
        return rolSpeler4;
    }

}
