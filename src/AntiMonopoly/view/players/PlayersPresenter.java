package AntiMonopoly.view.players;

import AntiMonopoly.model.AntiMonopolyModel;
import AntiMonopoly.model.Game;
import AntiMonopoly.model.PlayerRole;
import AntiMonopoly.model.Speler;
import AntiMonopoly.view.about.AboutPresenter;
import AntiMonopoly.view.about.AboutView;
import AntiMonopoly.view.game.*;
import AntiMonopoly.view.start.StartView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

public class PlayersPresenter {
    private AntiMonopolyModel model;
    private PlayersView view;

    public PlayersPresenter(
            AntiMonopolyModel model,
            PlayersView view) {
        this.model = model;
        this.view = view;
        this.addEventHandlers();
        this.updateView();
    }

    private void addEventHandlers() {
        view.getSpeler1().setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if ((view.getSpeler1().getText() != null && !view.getSpeler1().getText().isEmpty())) {
                    view.getRolSpeler1().setVisible(true);
                    view.getRolSpeler1().setPromptText("Kies een rol voor " + view.getSpeler1().getText());
                }
                if ((view.getSpeler1().getText().isEmpty())) {
                    view.getRolSpeler1().setVisible(false);
                    view.getRolSpeler2().setVisible(false);
                    view.getSpeler2().clear();
                    view.getRolSpeler3().setVisible(false);
                    view.getSpeler3().clear();
                    view.getSpeler3().setEditable(false);
                    view.getSpeler3().setStyle("-fx-background-color: grey;");
                    view.getRolSpeler4().setVisible(false);
                    view.getSpeler4().clear();
                    view.getSpeler4().setEditable(false);
                    view.getSpeler4().setStyle("-fx-background-color: grey;");
                }
                if ((view.getSpeler1().getText() != null && !view.getSpeler1().getText().isEmpty())
                        && view.getSpeler2().getText() != null && !view.getSpeler2().getText().isEmpty()) {
                    view.getSpeler3().setEditable(true);
                    view.getSpeler3().setStyle("-fx-background-color: none;");
                }
            }
        });
        view.getSpeler2().setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if ((view.getSpeler2().getText() != null && !view.getSpeler2().getText().isEmpty())) {
                    view.getRolSpeler2().setVisible(true);
                    view.getRolSpeler2().setPromptText("Kies een rol voor " + view.getSpeler2().getText());
                }
                if ((view.getSpeler2().getText().isEmpty())) {
                    view.getRolSpeler2().setVisible(false);
                    view.getRolSpeler3().setVisible(false);
                    view.getSpeler3().clear();
                    view.getSpeler3().setEditable(false);
                    view.getSpeler3().setStyle("-fx-background-color: grey;");
                    view.getRolSpeler4().setVisible(false);
                    view.getSpeler4().clear();
                    view.getSpeler4().setEditable(false);
                    view.getSpeler4().setStyle("-fx-background-color: grey;");
                }
                if ((view.getSpeler1().getText() != null && !view.getSpeler1().getText().isEmpty())
                        && view.getSpeler2().getText() != null && !view.getSpeler2().getText().isEmpty()) {
                    view.getSpeler3().setEditable(true);
                    view.getSpeler3().setStyle("-fx-background-color: none;");
                }
            }
        });
        view.getSpeler3().setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if ((view.getSpeler3().getText() != null && !view.getSpeler3().getText().isEmpty())) {
                    view.getRolSpeler3().setVisible(true);
                    view.getRolSpeler3().setPromptText("Kies een rol voor " + view.getSpeler3().getText());
                }
                if ((view.getSpeler3().getText().isEmpty())) {
                    view.getRolSpeler3().setVisible(false);
                    view.getRolSpeler4().setVisible(false);
                    view.getSpeler4().clear();
                    view.getSpeler4().setEditable(false);
                    view.getSpeler4().setStyle("-fx-background-color: grey;");
                }
                if (view.getSpeler1().getText() != null && !view.getSpeler1().getText().isEmpty()
                        && view.getSpeler2().getText() != null && !view.getSpeler2().getText().isEmpty()
                        && view.getSpeler3().getText() != null && !view.getSpeler3().getText().isEmpty()
                ) {
                    view.getSpeler4().setEditable(true);
                    view.getSpeler4().setStyle("-fx-background-color: none;");
                }
            }
        });
        view.getSpeler4().setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if ((view.getSpeler4().getText() != null && !view.getSpeler4().getText().isEmpty())) {
                    view.getRolSpeler4().setVisible(true);
                    view.getRolSpeler4().setPromptText("Kies een rol voor " + view.getSpeler4().getText());
                }
                if ((view.getSpeler4().getText().isEmpty())) {
                    view.getRolSpeler4().setVisible(false);
                }
            }
        });
        view.getButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //Controle aantal spelers
                if ((view.getSpeler1().getText().isEmpty()) || view.getSpeler2().getText().isEmpty()) {
                    final Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Opgelet!");
                    alert.setHeaderText("Te weinig spelers");
                    alert.setContentText("Er moeten minstens twee spelers mee doen!");
                    alert.showAndWait();
                } else if ((view.getSpeler1().getText() != null && !view.getSpeler1().getText().isEmpty() && view.getRolSpeler1().getSelectionModel().isEmpty())
                        || (view.getSpeler2().getText() != null && !view.getSpeler2().getText().isEmpty() && view.getRolSpeler2().getSelectionModel().isEmpty())
                        || (view.getSpeler3().getText() != null && !view.getSpeler3().getText().isEmpty() && view.getRolSpeler3().getSelectionModel().isEmpty())
                        || (view.getSpeler4().getText() != null && !view.getSpeler4().getText().isEmpty() && view.getRolSpeler4().getSelectionModel().isEmpty())) {
                    final Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Opgelet!");
                    alert.setHeaderText("Geen Rol Gekozen");
                    alert.setContentText("Kies voor elke speler een rol!");
                    alert.showAndWait();
                } else {
                    //aanmaken Game en Spelers
                    Game game = new Game(maakspelersaan());

                    //Opstarten GameView
                    GameView gameView = new GameView();
                    GamePresenter gamePresenter = new GamePresenter(game, gameView);
                    view.getScene().setRoot(gameView);
                    //gameView.getScene().getWindow().sizeToScene();
                }
            }
        });
    }


    private LinkedList<Speler> maakspelersaan() {
        //Aanmaken spelers
        Speler speler1 = new Speler(
                view.getSpeler1().getText(),
                PlayerRole.valueOf(view.getRolSpeler1().getValue()),
                true,
                Color.GREEN);
        LinkedList<Speler> spelers = new LinkedList<Speler>();
        spelers.add(speler1);
        if (view.getSpeler2().getText() != null && !view.getSpeler2().getText().isEmpty()) {
            Speler speler2 = new Speler(
                    view.getSpeler2().getText(),
                    PlayerRole.valueOf(view.getRolSpeler2().getValue()),
                    false,
                    Color.RED);
            spelers.add(speler2);
        }
        if (view.getSpeler3().getText() != null && !view.getSpeler3().getText().isEmpty()) {
            Speler speler3 = new Speler(
                    view.getSpeler3().getText(),
                    PlayerRole.valueOf(view.getRolSpeler3().getValue()),
                    false,
                    Color.BLUE);

            spelers.add(speler3);
        }
        if (view.getSpeler4().getText() != null && !view.getSpeler4().getText().isEmpty()) {
            Speler speler4 = new Speler(
                    view.getSpeler4().getText(),
                    PlayerRole.valueOf(view.getRolSpeler4().getValue()),
                    false,
                    Color.YELLOW);
            spelers.add(speler4);
        }
        return spelers;
    }

    private void updateView() {
// Vult de view met data uit model
    }
}
