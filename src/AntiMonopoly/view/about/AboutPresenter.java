package AntiMonopoly.view.about;

import AntiMonopoly.model.AntiMonopolyModel;
import AntiMonopoly.model.Game;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class AboutPresenter {
    public AboutPresenter(Game model, AboutView view) {
        view.getBtnOkeekes().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                view.getScene().getWindow().hide();
            }
        });
    }
}
