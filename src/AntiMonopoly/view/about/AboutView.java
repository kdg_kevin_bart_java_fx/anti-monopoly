package AntiMonopoly.view.about;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

public class AboutView extends BorderPane {
    private Button btnOk;

    public AboutView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        btnOk = new Button("Ok");
        btnOk.setPrefWidth(60);
    }

    private void layoutNodes() {
        setCenter(new Label("Deze applicatie is geschreven door Kevin Ringoet en Bart Vermeiren."));
        setPadding(new Insets(10));
        BorderPane.setAlignment(btnOk, Pos.CENTER_RIGHT);
        BorderPane.setMargin(btnOk, new Insets(10, 0, 0, 0));
        setBottom(btnOk);
    }

    Button getBtnOkeekes() {
        return btnOk;
    }
}
