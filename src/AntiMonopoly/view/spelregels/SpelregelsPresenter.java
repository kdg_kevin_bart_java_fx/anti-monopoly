package AntiMonopoly.view.spelregels;

import AntiMonopoly.model.Game;
import AntiMonopoly.model.Spelregels;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;

public class SpelregelsPresenter {
    public SpelregelsPresenter(Game model, SpelregelsView view) {
        try {
            String spelregels = new Spelregels().getRules();
            view.getTaRules().setText(spelregels);
        } catch (Exception me) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(me.getMessage());
            alert.showAndWait();
        }
    }
}
