package AntiMonopoly.view.start;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

public class StartView extends BorderPane {
    private Button btnStartGame;

    public StartView() {
        this.initialiseNodes();
        this.layoutNodes();
    }
    private void initialiseNodes() {
        btnStartGame = new Button("Start het spel");
    }
    private void layoutNodes() {
        Image image = new Image("/images/MonopolyMan.png");
        BackgroundSize backgroundSize =
                new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false,
                        false, false, false);
        Background background =
                new Background(new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize));
        this.setBackground(background);
        this.setCenter(btnStartGame);
    }

    Button getButton() {
        return btnStartGame;
    }


}
