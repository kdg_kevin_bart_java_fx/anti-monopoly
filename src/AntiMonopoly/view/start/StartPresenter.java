package AntiMonopoly.view.start;

import AntiMonopoly.model.AntiMonopolyModel;
import AntiMonopoly.view.game.GamePresenter;
import AntiMonopoly.view.game.GameView;
import AntiMonopoly.view.players.PlayersPresenter;
import AntiMonopoly.view.players.PlayersView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.WindowEvent;

import java.util.Optional;

public class StartPresenter {
    private AntiMonopolyModel model;
    private StartView view;

    public StartPresenter(
            AntiMonopolyModel model,
            StartView view) {
        this.model = model;
        this.view = view;
        this.addEventHandlers();
        this.updateView();
    }

    private void addEventHandlers() {
        view.getButton().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                PlayersView playersView = new PlayersView();
                PlayersPresenter playersPresenter = new PlayersPresenter(model, playersView);
                view.getScene().setRoot(playersView);
                playersView.getScene().getWindow().sizeToScene();
            }
        });
    }

    private void updateView() {
// Vult de view met data uit model
    }

    public void addWindowEventHandlers() {
        view.getScene().getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                final Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Opgelet!");
                alert.setHeaderText("Hierdoor stopt het spel!");
                alert.setContentText("Ben je zeker?");
                alert.getButtonTypes().clear();
                ButtonType neen = new ButtonType("Neen");
                ButtonType ja = new ButtonType("ja");
                alert.getButtonTypes().addAll(neen, ja);
                alert.showAndWait();
                if(alert.getResult()==null||alert.getResult().equals(neen)){
                    event.consume();
                }
            }
        });
    }
}
