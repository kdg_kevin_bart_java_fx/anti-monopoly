package AntiMonopoly.view.game;

import AntiMonopoly.model.Straat;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;

import java.util.ArrayList;
import java.util.List;

public class GameView extends BorderPane {
    private MenuItem miAbout, spelregels;
    private Label lblNaamSpeler1, lblNaamSpeler2, lblNaamSpeler3, lblNaamSpeler4,
            lblBedragSpeler1, lblBedragSpeler2, lblBedragSpeler3, lblBedragSpeler4,
            lblActieveSpeler;
    private Button btnDobbel, btnBeurtDoorgeven, btnKopen, btnHuurBetalen;
    private BoardView board;
    private BorderPane straatView;

    public GameView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        miAbout = new MenuItem("about");
        spelregels = new Menu("spelregels-info");
        this.lblNaamSpeler1 = new Label();
        this.lblNaamSpeler2 = new Label();
        this.lblNaamSpeler3 = new Label();
        this.lblNaamSpeler4 = new Label();
        this.lblBedragSpeler1 = new Label();
        this.lblBedragSpeler2 = new Label();
        this.lblBedragSpeler3 = new Label();
        this.lblBedragSpeler4 = new Label();
        this.lblActieveSpeler = new Label();
        this.btnDobbel = new Button();
        this.btnBeurtDoorgeven = new Button("Beurt doorgeven");
        this.btnKopen = new Button();
        this.btnHuurBetalen = new Button();
        this.board = new BoardView();
        this.straatView = new BorderPane();
    }

    private void layoutNodes() {
        //Board
        setCenter(board);
        //Stratenoverzicht
        straatView.setPrefWidth(500);
        straatView.setPrefHeight(Double.MAX_VALUE);
        setRight(straatView);
        //Game Status
        VBox gameStatusView = new VBox();
        this.btnDobbel.setMaxWidth(Double.MAX_VALUE);
        this.btnDobbel.setMaxHeight(100);
        VBox.setVgrow(btnDobbel, Priority.ALWAYS);
        gameStatusView.setPrefWidth(200);
        gameStatusView.setSpacing(20);
        gameStatusView.setPadding(new Insets(20));
        gameStatusView.getChildren().
                addAll(lblActieveSpeler, lblNaamSpeler1, lblBedragSpeler1,lblNaamSpeler2, lblBedragSpeler2,
                        lblNaamSpeler3, lblBedragSpeler3, lblNaamSpeler4, lblBedragSpeler4,
                        btnDobbel, btnBeurtDoorgeven, btnKopen, btnHuurBetalen);
        lblActieveSpeler.setFont(new Font(16));
        lblActieveSpeler.setUnderline(true);
        //labelsSpelersnameOpInvisible
        lblNaamSpeler1.setVisible(false);
        lblNaamSpeler2.setVisible(false);
        lblNaamSpeler3.setVisible(false);
        lblNaamSpeler4.setVisible(false);
        lblBedragSpeler1.setVisible(false);
        lblBedragSpeler2.setVisible(false);
        lblBedragSpeler3.setVisible(false);
        lblBedragSpeler4.setVisible(false);
        btnKopen.setVisible(false);
        btnHuurBetalen.setVisible(false);

        gameStatusView.setAlignment(Pos.TOP_LEFT);
        this.setLeft(gameStatusView);

        //Menu
        Menu mnHelp = new Menu("Help");
        mnHelp.getItems().addAll(miAbout, spelregels);
        MenuBar menuBar = new MenuBar(mnHelp);
        setTop(menuBar);
    }

    //getters
    MenuItem getMiAbout() {
        return miAbout;
    }

    MenuItem getSpelregels() {
        return spelregels;
    }

    Label getLblNaamSpeler1() {
        return lblNaamSpeler1;
    }

    Label getLblNaamSpeler2() {
        return lblNaamSpeler2;
    }

    Label getLblNaamSpeler3() {
        return lblNaamSpeler3;
    }

    Label getLblNaamSpeler4() {
        return lblNaamSpeler4;
    }

    Label getLblBedragSpeler1() {
        return lblBedragSpeler1;
    }

    Label getLblBedragSpeler2() {
        return lblBedragSpeler2;
    }

    Label getLblBedragSpeler3() {
        return lblBedragSpeler3;
    }

    Label getLblBedragSpeler4() {
        return lblBedragSpeler4;
    }

    Label getLblActieveSpeler() {
        return lblActieveSpeler;
    }

    Button getBtnDobbel() {
        return btnDobbel;
    }
    Button getBtnBeurtDoorgeven(){return btnBeurtDoorgeven;}

    BoardView getBoard() {
        return board;
    }

    Button getBtnKopen(){return btnKopen;}
    Button getBtnHuurBetalen(){return btnHuurBetalen;}

    BorderPane getStraatView() {
        return straatView;
    }

    void setStraatView(StraatView straatView) {
        this.straatView = straatView;
    }
}
