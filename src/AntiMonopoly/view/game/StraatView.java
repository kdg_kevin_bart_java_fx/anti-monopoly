package AntiMonopoly.view.game;

import AntiMonopoly.model.Straat;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.util.List;

public class StraatView extends BorderPane {
    private TableView<Straat> table = new TableView<>();
    private Label lblOverzicht;
    private List <Straat> straten;


    public StraatView (List <Straat> straten) {
        this.straten=straten;
        this.initialiseNodes();
        this.layoutNodes();

    }
    private void initialiseNodes() {
        lblOverzicht = new Label ("Overzicht straten");
    }
    private void layoutNodes() {
        table.setEditable(true);
        table.getItems().addAll(straten);
        TableColumn<Straat, String> straatNaamCol = new TableColumn<>("Straatnaam");
        straatNaamCol.setCellValueFactory(c-> new SimpleStringProperty(c.getValue().getStraatNaam()));
        TableColumn<Straat, String> stadCol = new TableColumn<>("Stad");
        stadCol.setCellValueFactory(c-> new SimpleStringProperty(c.getValue().getStad().toString()));
        TableColumn<Straat, String> eigenaarCol = new TableColumn<>("Eigenaar");
        eigenaarCol.setCellValueFactory
                (c-> new SimpleStringProperty((c.getValue().getEigenaar())!=null?c.getValue().getEigenaar().getNaam():"geen eigenaar"));
        TableColumn<Straat, String> vaknummerCol = new TableColumn<>("Vak");
        vaknummerCol.setCellValueFactory(c-> new SimpleStringProperty(Integer.toString(c.getValue().getVakNummer())));
        TableColumn<Straat, String> monopolieCol = new TableColumn<>("Monopolie");
        monopolieCol.setCellValueFactory(c-> new SimpleStringProperty(Boolean.toString(c.getValue().isMonopoliePositie())));
        TableColumn<Straat, String> huizenCol = new TableColumn<>("Huizen");
        huizenCol.setCellValueFactory
                (c-> new SimpleStringProperty((c.getValue().isHotel())?"Hotel":Integer.toString(c.getValue().getAantalHuizen())));
        //table.setItems(FXCollections.observableList(straten));
        table.getColumns().addAll(vaknummerCol,straatNaamCol, stadCol, eigenaarCol, monopolieCol, huizenCol);


        VBox box = new VBox();
        box.setPrefWidth(500);
        box.setPrefHeight(Double.MAX_VALUE);
        box.getChildren().addAll(lblOverzicht, table);
        this.setCenter(box);
        this.setPrefHeight(Double.MAX_VALUE);

    }

    Label getLblOverzicht() {
        return lblOverzicht;
    }

    TableView getTable() {
        return table;
    }

    void setTable(TableView table) {
        this.table = table;
    }

    List<Straat> getStraten() {
        return straten;
    }

    void setStraten(List<Straat> straten) {
        this.straten = straten;
    }
}

