package AntiMonopoly.view.game;

import AntiMonopoly.model.Pion;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class BoardView extends GridPane {

    public BoardView() {
        initNodes();
        layoutNodes();
    }

    private void initNodes(){

    }

    private void layoutNodes(){
        //grid aanmaken
        maakgrid();
        //grid labellen met vaknumers
        zetVaknummer();
        //gridlijnen tonen of verbergen
        this.setGridLinesVisible(false);
        //SetBackground
        zetAchtergrond();
    }

    private void maakgrid() {
        RowConstraints rowsEdge = new RowConstraints();
        rowsEdge.setPercentHeight(14);
        RowConstraints rowsMid = new RowConstraints();
        rowsMid.setPercentHeight(8);

        ColumnConstraints colEdge = new ColumnConstraints();
        colEdge.setPercentWidth(14);

        ColumnConstraints colMid = new ColumnConstraints();
        colMid.setPercentWidth(8);

        this.getColumnConstraints().addAll(colEdge, colMid,
                colMid, colMid, colMid, colMid, colMid, colMid, colMid, colMid, colEdge);
        this.getRowConstraints().addAll(rowsEdge, rowsMid,
                rowsMid, rowsMid, rowsMid, rowsMid, rowsMid, rowsMid, rowsMid, rowsMid, rowsEdge);
    }

    private void zetAchtergrond() {
        Image image = new Image("/images/monopolyBoard.jpg");
        BackgroundImage backgroundImage = new BackgroundImage(
                image,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                //new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO,false,false,true,false)
                new BackgroundSize(1,1,true,true,false, false));
        this.setBackground(new Background(backgroundImage));
        //this.setStyle("-fx-background-image: url('/images/monopolyBoard.jpg')");
    }

    private void zetVaknummer() {
        //Vaknummers
        int j = 1;
        for (int i = 10; i>0; i--){
            String vaknummer = String.valueOf(j++);
            Label label = new Label(vaknummer);
            //ipv aparte pionImage zou vak gekleurd kunnen worden als er een speler op stat
            // label.setBackground(new Background(new BackgroundFill(Color.YELLOW,CornerRadii.EMPTY,Insets.EMPTY)));
            this.add(label, i, 10);
            label.setPadding(new Insets(0,0,0,10));
        }
        for (int i = 10; i>0; i--){
            String vaknummer = String.valueOf(j++);
            Label label = new Label(vaknummer);
            this.add(label, 0, i);
            label.setPadding(new Insets(0,0,0,10));
        }
        for (int i = 0; i<10; i++){
            String vaknummer = String.valueOf(j++);
            Label label = new Label(vaknummer);
            this.add(label, i, 0);
            label.setPadding(new Insets(0,0,0,10));
        }
        for (int i = 0; i<10; i++){
            String vaknummer = String.valueOf(j++);
            Label label = new Label(vaknummer);
            this.add(label, 10, i);
            label.setPadding(new Insets(0,10,0,0));
        }
    }
}
