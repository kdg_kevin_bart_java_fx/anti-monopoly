package AntiMonopoly.view.game;

import AntiMonopoly.model.*;
import AntiMonopoly.view.about.AboutPresenter;
import AntiMonopoly.view.about.AboutView;
import AntiMonopoly.view.spelregels.SpelregelsPresenter;
import AntiMonopoly.view.spelregels.SpelregelsView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.management.relation.Role;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static AntiMonopoly.model.Stad.LEUVEN;

public class GamePresenter {
    private GameView view;
    private Game model;
    private StraatView straatView;

    public GamePresenter(Game model, GameView view) {
        this.view = view;
        this.model = model;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        view.getBtnDobbel().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateView();
                bepaalAantalOgenEnVerplaatsPion();
                bepaalActieOpBasisVanNieuwePostie();
            }
        });
        view.getBtnBeurtDoorgeven().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateActieveSpeler();
                updateView();

            }
        });

        view.getBtnKopen().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                view.getBtnKopen().setVisible(false);
                int vakNummer = model.getSpelers().get(model.getBord().getActieveSpelerID()).getHuidigVakNummer();
                Straat straat = (Straat) model.getBord().getVakken().get(vakNummer - 1);
                model.getSpelers().get(model.getBord().getActieveSpelerID()).getStratenInBezit().add(straat);
                if (straat.getGrondPrijs() <= model.getSpelers().get(model.getBord().getActieveSpelerID()).getVermogen()) {
                    model.getSpelers().get(model.getBord().getActieveSpelerID()).getStratenInBezit().add(straat);
                    model.getSpelers().get(model.getBord().getActieveSpelerID()).betaalBedrag(straat.getGrondPrijs());
                    straat.setEigenaar(model.getSpelers().get(model.getBord().getActieveSpelerID()));
                } else {
                    final Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Opgelet!");
                    alert.setHeaderText("Onvoldoende geld");
                    alert.setContentText("U kan " + straat.getStraatNaam() + " niet kopen u bezit minder dan " + straat.getGrondPrijs() + ".");
                    alert.showAndWait();
                }
                //als speler niet meer kan gooien (dus geen zes had gegooid), gaat beurt naar volgende speler
                if(view.getBtnDobbel().isDisabled()){
                    updateActieveSpeler();
                    updateView();};
                updateMonopoliePostie(straat);
            }
        });

        view.getBtnHuurBetalen().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                view.getBtnHuurBetalen().setVisible(false);
                int vakNummer = model.getSpelers().get(model.getBord().getActieveSpelerID()).getHuidigVakNummer();
                Straat straat = (Straat) model.getBord().getVakken().get(vakNummer - 1);
                if (straat.getHuurPrijs() <= model.getSpelers().get(model.getBord().getActieveSpelerID()).getVermogen()) {
                    model.getSpelers().get(model.getBord().getActieveSpelerID()).betaalBedrag(straat.getHuurPrijs());
                    straat.getEigenaar().ontvangBedrag(straat.getHuurPrijs());
                } else {
                    final Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Opgelet!");
                    alert.setHeaderText("Onvoldoende geld");
                    alert.setContentText("U bent failliet!");
                    alert.showAndWait();
                }
                updateActieveSpeler();
                updateView();
            }
        });

        view.getMiAbout().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                AboutView aboutView = new AboutView();
                AboutPresenter aboutPresenter = new AboutPresenter(model, aboutView);
                Stage aboutStage = new Stage();
                aboutStage.initOwner(view.getScene().getWindow());
                aboutStage.initModality(Modality.APPLICATION_MODAL);
                aboutStage.setScene(new Scene(aboutView));
                aboutStage.setX(view.getScene().getWindow().getX() + 0);
                aboutStage.setY(view.getScene().getWindow().getY() + 0);
                aboutStage.showAndWait();
            }
        });

        view.getSpelregels().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                SpelregelsView spelregelsView = new SpelregelsView();
                SpelregelsPresenter spelregelsPresenter = new SpelregelsPresenter(model, spelregelsView);
                Stage aboutStage = new Stage();
                aboutStage.initOwner(view.getScene().getWindow());
                aboutStage.initModality(Modality.APPLICATION_MODAL);
                aboutStage.setScene(new Scene(spelregelsView));
                aboutStage.setX(view.getScene().getWindow().getX() + 0);
                aboutStage.setY(view.getScene().getWindow().getY() + 0);
                aboutStage.showAndWait();
            }
        });
    }

    private void updateView() {
        //Vul de namen en bedragen in
        int aantalSpelers = model.getBord().getAantalSpelers();
        for (int i = 0; i < aantalSpelers; i++) {
            getLabelOpBasisVanSpelerId(i).
                    setText(this.model.getSpelers().get(i).getNaam() + " (" + this.model.getSpelers().get(i).getRol().name() + ")");
            getLabelOpBasisVanSpelerId(i).setVisible(true);
            getLabelOpBasisVanSpelerId(i).setTextFill(Color.BLACK);
            getBedragLabelOpBasisVanSpelerId(i).setText(String.valueOf(this.model.getSpelers().get(i).getVermogen()));
            getBedragLabelOpBasisVanSpelerId(i).setVisible(true);
            getBedragLabelOpBasisVanSpelerId(i).setTextFill(Color.BLACK);
        }
        //haal actieve spelerID op
        int actieveSpelerId = model.getBord().getActieveSpelerID();
        //actieve speler licht op en komt in textLabel
        view.getLblActieveSpeler().setText(this.model.getSpelers().get(actieveSpelerId).getNaam() + " is aan de beurt.");
        view.getBtnDobbel().setText(this.model.getSpelers().get(actieveSpelerId).getNaam() + ", klik hier om de dobbelsteen te werpen");
        view.getBtnDobbel().setDisable(false);
        view.getBtnBeurtDoorgeven().setDisable(true);
        getLabelOpBasisVanSpelerId(actieveSpelerId).setTextFill(this.model.getSpelers().get(actieveSpelerId).getColor());
        getBedragLabelOpBasisVanSpelerId(actieveSpelerId).setTextFill(this.model.getSpelers().get(actieveSpelerId).getColor());

        view.getBtnDobbel().setText(this.model.getSpelers().get(actieveSpelerId).getNaam() + ", klik hier om de dobbelsteen te werpen");
        view.getBtnKopen().setVisible(false);

        //StraatView aanmaken
        ArrayList<Straat> straten = model.getBord()
                .getVakken()
                .stream()
                .filter(v -> v instanceof Straat)
                .map(Straat.class::cast)
                .collect(Collectors.toCollection(ArrayList::new));

        straatView = new StraatView(straten);
        view.setStraatView(straatView);
        view.setRight(straatView);


    /*
        this.view.getTimeLabel().setText("Time: " + this.model.getSeconds() + " seconds");

    Score score = this.model.getScoreManager().getHighScore(this.model.getDifficulty());
    String bestTimeText;
    String bestDateText;
        if (score != null) {
        bestTimeText = "Best time: " + score.getSeconds() + " seconds";
        bestDateText = "Date: " + score.getDate();
    } else {
        bestTimeText = "";
        bestDateText = "";
    */
    }

    private Label getLabelOpBasisVanSpelerId(int spelerId) {
        Label label = new Label();
        switch (spelerId) {
            case 0:
                label = view.getLblNaamSpeler1();
                break;
            case 1:
                label = view.getLblNaamSpeler2();
                break;
            case 2:
                label = view.getLblNaamSpeler3();
                break;
            case 3:
                label = view.getLblNaamSpeler4();
                break;
        }
        return label;
    }

    private Label getBedragLabelOpBasisVanSpelerId(int spelerId) {
        Label label = new Label();
        switch (spelerId) {
            case 0:
                label = view.getLblBedragSpeler1();
                break;
            case 1:
                label = view.getLblBedragSpeler2();
                break;
            case 2:
                label = view.getLblBedragSpeler3();
                break;
            case 3:
                label = view.getLblBedragSpeler4();
                break;
        }
        return label;
    }

    private void updateActieveSpeler() {
        int nieuweActieveSpeler = model.getBord().getActieveSpelerID() + 1;
        if (nieuweActieveSpeler < model.getBord().getAantalSpelers()) {
            model.getBord().setActieveSpelerID(nieuweActieveSpeler);
        } else {
            model.getBord().setActieveSpelerID(nieuweActieveSpeler - model.getBord().getAantalSpelers());
        }
        ;
    }

    private void bepaalAantalOgenEnVerplaatsPion() {
        int aantalOgen = new Random().nextInt(6) + 1;
        view.getBtnDobbel().setText("U gooide " + aantalOgen);
        if (aantalOgen == 6) {
            view.getBtnDobbel().setText("U gooide 6, u mag nog een keer gooien");
            view.getBtnBeurtDoorgeven().setDisable(true);
        } else {
            view.getBtnDobbel().setDisable(true);
            view.getBtnBeurtDoorgeven().setDisable(false);
        }
        int huidigVakNummer = model.getSpelers().get(model.getBord().getActieveSpelerID()).getHuidigVakNummer();
        int nieuwePositie;
        if (huidigVakNummer + aantalOgen <= Bord.AANTALVAKKEN) {
            nieuwePositie = huidigVakNummer + aantalOgen;
        } else {
            nieuwePositie = huidigVakNummer + aantalOgen - Bord.AANTALVAKKEN;
            model.getSpelers().get(model.getBord().getActieveSpelerID()).voegRondegeldToeAanVermogen();
        }

        verplaatsPion(model.getBord().getActieveSpelerID(), huidigVakNummer, nieuwePositie);
        model.getSpelers().get(model.getBord().getActieveSpelerID()).setHuidigVakNummer(nieuwePositie);
    }


    private void verplaatsPion(int spelerID, int huidigVakNummer, int nieuwVaknummmer) {
        int column = model.getBord().getVakken().get(huidigVakNummer - 1).getColumn();
        int row = model.getBord().getVakken().get(huidigVakNummer - 1).getRow();
        ObservableList<Node> childrens = view.getBoard().getChildren();
        for (Node node : childrens) {
            if (node instanceof ImageView && view.getBoard().getRowIndex(node) == row && view.getBoard().getColumnIndex(node) == column) {
                //ImageView imageView=ImageView(node); // use what you want to remove
                view.getBoard().getChildren().remove(node);
                break;
            }
        }
        plaatsPion(spelerID, nieuwVaknummmer);
    }

    private void bepaalActieOpBasisVanNieuwePostie() {
        int vakNummer = model.getSpelers().get(model.getBord().getActieveSpelerID()).getHuidigVakNummer();
        //checken of het een straat is
        if (model.getBord().getVakken().get(vakNummer - 1).getClass() == Straat.class) {
            Straat straat = (Straat) model.getBord().getVakken().get(vakNummer - 1);

            //als de straat nog vrij is, kan deze gekocht worden
            if (straat.getEigenaar() == null) {
                view.getBtnKopen().setVisible(true);
                view.getBtnKopen().setDisable(false);
                view.getBtnKopen().setText(straat.getStraatNaam() + "\n kopen voor " +
                        straat.getGrondPrijs() + "?");
            }

            //straat reeds in bezit
            else if(straat.getEigenaar().equals(model.getSpelers().get(model.getBord().getActieveSpelerID()))){
                view.getBtnKopen().setVisible(true);
                view.getBtnKopen().setText(straat.getStraatNaam() + "\n is reeds in uw bezit");
                view.getBtnKopen().setDisable(true);
            }
            //straat in bezit van iemand anders, huur betalen
            else {
                view.getBtnBeurtDoorgeven().setDisable(true);
                view.getBtnHuurBetalen().setVisible(true);
                //dubbel huur indien monopolist die volledige stad bezit
                if (straat.isMonopoliePositie()) {
                    view.getBtnHuurBetalen().setText("Betaal dubbele huur:\n 2x " + straat.getHuurPrijs() + " huur \n aan monopolist " +
                            straat.getEigenaar().getNaam());
                } else {
                    view.getBtnHuurBetalen().setText("Betaal " + straat.getHuurPrijs() + " huur \n aan " +
                            straat.getEigenaar().getNaam());
                }

            }
        } else {
            view.getBtnBeurtDoorgeven().setText("Beurt doorgeven");
        }
    }


    private void plaatsPion(int spelerID, int vakNummer) {
        ImageView imageView = new ImageView((new Image(this.model.getSpelers().get(spelerID).getPion().getImage().getUrl())));
        imageView.setFitHeight(40);
        imageView.setFitWidth(40);
        //we maken van vaknummer een index door 1 af te rekken, index begin bij 0 vakken bij 1
        int column = model.getBord().getVakken().get(vakNummer - 1).getColumn();
        int row = model.getBord().getVakken().get(vakNummer - 1).getRow();

        view.getBoard().add(imageView, column, row);
        view.getBoard().setPadding(new Insets(5));
    }

    private void updateMonopoliePostie(Straat straat) {
        Stad stad = straat.getStad();
        Speler eigenaar = straat.getEigenaar();
        //Een monopolie wordt hier gezet als alle straten van een stad dezelfde eigenaar hebben
        //TODO aan te passen naar minimum twee straten voor monopolie (ipv alle)
        boolean heeftMonopolie = true;

        ArrayList<Straat> stratenInDeStad = model.getBord()
                .getVakken()
                .stream()
                .filter(v -> v instanceof Straat && ((Straat) v).getStad().equals(stad))
                .map(Straat.class::cast)
                .collect(Collectors.toCollection(ArrayList::new));
        for (Straat s : stratenInDeStad) {
            if (s.getEigenaar() == null || !s.getEigenaar().equals(eigenaar)||eigenaar.getRol().equals(PlayerRole.CONCURRENT)) {
                heeftMonopolie = false;
            }
        }
        for (Straat s : stratenInDeStad) {
            s.setMonopoliePositie(heeftMonopolie);
        }
    }


}

