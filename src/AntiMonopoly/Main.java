package AntiMonopoly;

import AntiMonopoly.model.AntiMonopolyModel;
import AntiMonopoly.view.start.StartPresenter;
import AntiMonopoly.view.start.StartView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        AntiMonopolyModel model =
                new AntiMonopolyModel();
        StartView view =
                new StartView();
        StartPresenter presenter = new StartPresenter(model, view);

        /*Menu m = new Menu("Menu");

        // create menuitems
        MenuItem m1 = new MenuItem("menu item 1");
        MenuItem m2 = new MenuItem("menu item 2");
        MenuItem m3 = new MenuItem("menu item 3");

        // add menu items to menu
        m.getItems().add(m1);
        m.getItems().add(m2);
        m.getItems().add(m3);

        // create a menubar
        MenuBar mb = new MenuBar();

        // add menu to menubar
        mb.getMenus().add(m);

        // create a VBox
        VBox vb = new VBox(mb);

*/

        primaryStage.setScene(new Scene(view, 800,600));
        primaryStage.setTitle("Anti-Monopolie");
        presenter.addWindowEventHandlers();
        primaryStage.show();
    }
}
