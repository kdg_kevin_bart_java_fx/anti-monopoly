package AntiMonopoly.model;

import java.util.Collection;
import java.util.LinkedList;

public class Game {
    private LinkedList<Speler> spelers = new LinkedList<Speler>();
    private Bord bord;

    public Game(LinkedList<Speler> spelers) {
        this.spelers = spelers;
        bord=new Bord(0, spelers.size());
    }

    public void addSpeler(Speler speler){
            spelers.add(speler);
    }

    public LinkedList<Speler> getSpelers() {
        return spelers;
    }

    public void setSpelers(LinkedList<Speler> spelers) {
        this.spelers = spelers;
    }

    public Bord getBord() {
        return bord;
    }

    public void setBord(Bord bord) {
        this.bord = bord;
    }
}
