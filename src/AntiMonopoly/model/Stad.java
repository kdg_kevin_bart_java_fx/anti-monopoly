package AntiMonopoly.model;

public enum Stad {
    OOSTENDE, LEUVEN, TURNHOUT, BRUGGE, GENT, HASSELT, ANTWERPEN, BRUSSEL
}
