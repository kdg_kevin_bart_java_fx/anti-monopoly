package AntiMonopoly.model;

public class Straat extends Cell {
    private String straatNaam;
    private Stad stad;
    private int grondPrijs;
    private Speler eigenaar;
    private int huurPrijs;
    private boolean monopoliePositie;
    private int aantalHuizen;
    private boolean hotel;

    public Straat(int vakNummer, int column, int row, String straatNaam, Stad stad, int grondPrijs, int huurPrijs) {
        super(vakNummer, column, row);
        this.straatNaam = straatNaam;
        this.stad = stad;
        this.grondPrijs = grondPrijs;
        this.huurPrijs = huurPrijs;
        this.aantalHuizen = 0;
        this.monopoliePositie = false;
        this.hotel = false;
    }

    public String getStraatNaam() {
        return straatNaam;
    }

    public void setStraatNaam(String straatNaam) {
        this.straatNaam = straatNaam;
    }

    public Stad getStad() {
        return stad;
    }

    public void setStad(Stad stad) {
        this.stad = stad;
    }

    public int getGrondPrijs() {
        return grondPrijs;
    }

    public void setGrondPrijs(int grondPrijs) {
        this.grondPrijs = grondPrijs;
    }

    public Speler getEigenaar() {
        return eigenaar;
    }

    public void setEigenaar(Speler eigenaar) {
        this.eigenaar = eigenaar;
    }

    public int getHuurPrijs() {
        return huurPrijs;
    }

    public void setHuurPrijs(int huurPrijs) {
        this.huurPrijs = huurPrijs;
    }

    public boolean isMonopoliePositie() {
        return monopoliePositie;
    }

    public void setMonopoliePositie(boolean monopoliePositie) {
        this.monopoliePositie = monopoliePositie;
    }

    public int getAantalHuizen() {
        return aantalHuizen;
    }

    public void setAantalHuizen(int aantalHuizen) {
        this.aantalHuizen = aantalHuizen;
    }

    public boolean isHotel() {
        return hotel;
    }

    public void setHotel(boolean hotel) {
        this.hotel = hotel;
    }
}


