package AntiMonopoly.model;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class Pion {
    private Image image;
    private Color color;
    private String url;

    public Pion(Color color) {
        this.color = color;
        setImage(color);
    }


    public Image getImage() {
        return image;
    }

    public void setImage(Color color)
    {
        if (color.equals(Color.BLUE)){
            image = new Image("/images/pawn_blue.png");
        }
        else if(color.equals(Color.GREEN)){
            image = new Image("/images/pawn_green.png");
        }
        else if(color.equals(Color.RED)){
            image = new Image("/images/pawn_red.png");
        }
        else if(color.equals(Color.YELLOW)){
            image = new Image("/images/pawn_yellow.png");
        }
        else {
            image = new Image("/images/pawn_grey.png");
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
