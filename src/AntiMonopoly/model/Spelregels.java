package AntiMonopoly.model;

import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Spelregels {

    public static final String SPELREGELS_FILE = "AntiMonopoly/resources/spelregels.txt";

    private String rules = "";

    public Spelregels() {

        try (FileInputStream fis = new FileInputStream(SPELREGELS_FILE);
             InputStreamReader isr = new InputStreamReader(fis,
                     StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(isr)) {

            String line;


            while ((line = br.readLine()) != null) {
                System.out.println(line);
                rules = rules + line + "\n";
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String getRules() {
        return rules;
    }
}


