package AntiMonopoly.model;

    public class Cell {
        private int vakNummer;
        private int column;
        private int row;

        public Cell(int vakNummer, int column, int row) {
            this.vakNummer = vakNummer;
            this.column = column;
            this.row = row;
        }

        public int getVakNummer() {
            return vakNummer;
        }

        public void setVakNummer(int vakNummer) {
            this.vakNummer = vakNummer;
        }

        public int getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        public int getRow() {
            return row;
        }

        public void setRow(int row) {
            this.row = row;
        }
    }
