package AntiMonopoly.model;

import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Objects;

public class Speler {
    private String naam;
    private PlayerRole rol;
    private boolean activePlayer;
    private Color color;
    private Pion pion;
    private int vermogen;
    private static final int STARTBEDRAG = 1500;
    private static final int RONDEBEDRAG = 100;
    private static final int STARTVAKNUMMER = 1;
    private int huidigVakNummer;
    private ArrayList<Straat> stratenInBezit;

    public Speler(String naam, PlayerRole rol, boolean activePlayer, Color color) {
        this.naam = naam;
        this.rol = rol;
        this.activePlayer = activePlayer;
        this.color = color;
        this.vermogen = STARTBEDRAG;
        pion = new Pion(color);
        huidigVakNummer = STARTVAKNUMMER;
        stratenInBezit = new ArrayList<>();
    }

    public void voegRondegeldToeAanVermogen(){
        vermogen = vermogen+RONDEBEDRAG;
    }

    public void betaalBedrag(int bedrag){
        vermogen = vermogen - bedrag;
    }

    public void ontvangBedrag(int bedrag){
        vermogen = vermogen + bedrag;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public PlayerRole getRol() {
        return rol;
    }

    public void setRol(PlayerRole rol) {
        this.rol = rol;
    }

    public boolean isActivePlayer() {
        return activePlayer;
    }

    public void setActivePlayer(boolean activePlayer) {
        this.activePlayer = activePlayer;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getVermogen() {
        return vermogen;
    }

    public void setVermogen(int vermogen) {
        this.vermogen = vermogen;
    }

    public Pion getPion() {
        return pion;
    }

    public void setPion(Pion pion) {
        this.pion = pion;
    }

    public int getHuidigVakNummer() {
        return huidigVakNummer;
    }

    public void setHuidigVakNummer(int huidigVakNummer) {
        this.huidigVakNummer =  huidigVakNummer;
    }

    public ArrayList<Straat> getStratenInBezit() {
        return stratenInBezit;
    }

    public void setStratenInBezit(ArrayList<Straat> stratenInBezit) {
        this.stratenInBezit = stratenInBezit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Speler speler = (Speler) o;
        return Objects.equals(naam, speler.naam) &&
                rol == speler.rol;
    }

    @Override
    public int hashCode() {
        return Objects.hash(naam, rol);
    }
}
