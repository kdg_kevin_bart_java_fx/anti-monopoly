package AntiMonopoly.model;

import javafx.beans.InvalidationListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import java.util.*;

public class Bord {
    private int actieveSpelerID ;
    private int aantalSpelers ;
    public static final int AANTALVAKKEN = 40;
    public static final int STARTVAKNUMMER = 1;
    List<Cell> vakken= new ArrayList<>();

    public Bord(int actieveSpelerID, int aantalSpelers) {
        this.actieveSpelerID = actieveSpelerID;
        this.aantalSpelers = aantalSpelers;
        this.vakken = maakVakkenAan();
    }

    private  ArrayList<Cell> maakVakkenAan() {
        ArrayList<Cell> cells = new ArrayList<Cell>();
        cells.add(new Cell(1,10,10));
        cells.add(new Straat(2,9,10,"Kustlaan", Stad.OOSTENDE,100, 10));
        cells.add(new Cell(3,8,10));
        cells.add(new Straat(4,7,10, "Duinenlaan", Stad.OOSTENDE, 100,10));
        cells.add(new Cell(5,6,10));
        cells.add(new Cell(6,5,10));
        cells.add(new Straat(7,4,10, "Vismarkt",Stad.LEUVEN,200,20));
        cells.add(new Cell(8,3,10));
        cells.add(new Straat(9,2,10,"Tiensevest",Stad.LEUVEN,200,20));
        cells.add(new Straat(10,1,10, "Bondgenotenlaan", Stad.LEUVEN, 200,20));
        cells.add(new Cell(11,0,10));
        cells.add(new Straat(12,0,9,"Leopoldstraat",Stad.TURNHOUT,300,30));
        cells.add(new Cell(13,0,8));
        cells.add(new Straat(14,0,7, "Begijnenstraat", Stad.TURNHOUT,300,30));
        cells.add(new Straat(15,0,6,"Gasthuisstraat",Stad.TURNHOUT,300,30));
        cells.add(new Cell(16,0,5));
        cells.add(new Straat(17,0,4,"Steenstraat",Stad.BRUGGE, 400,40));
        cells.add(new Cell(18,0,3));
        cells.add(new Straat(19,0,2, "Vlamingenstraat",Stad.BRUGGE, 400,40));
        cells.add(new Straat(20,0,1,"Kapelstraat",Stad.BRUGGE,400,40));
        cells.add(new Cell(21,0,0));
        cells.add(new Straat(22,1,0, "Kouter",Stad.GENT, 500,50));
        cells.add(new Cell(23,2,0));
        cells.add(new Straat(24,3,0,"Veldstraat",Stad.GENT,500,50));
        cells.add(new Straat(25,4,0, "Vlaanderenstraat",Stad.GENT,500,50));
        cells.add(new Cell(26,5,0));
        cells.add(new Straat(27,6,0,"Grote Markt",Stad.HASSELT,600,60));
        cells.add(new Straat(28,7,0,"Kerkstraat",Stad.HASSELT,  600,60));
        cells.add(new Cell(29,8,0));
        cells.add(new Straat(30,9,0,"Demerstraat",Stad.HASSELT,600,60));
        cells.add(new Cell(31,10,0));
        cells.add(new Straat(32,10,1,"Mechelseplein",Stad.ANTWERPEN,700,70));
        cells.add(new Straat(33,10,2, "Groenplaats",Stad.ANTWERPEN, 700,70));
        cells.add(new Cell(34,10,3));
        cells.add(new Straat(35,10,4,"Meir",Stad.ANTWERPEN, 700,70));
        cells.add(new Cell(36,10,5));
      cells.add(new Cell(37,10,6));
        cells.add(new Straat(38,10,7, "Lakenstraat", Stad.BRUSSEL,800,80));
        cells.add(new Cell(39,10,8));
        cells.add(new Straat(40,10,9, "Nieuwstraat",Stad.BRUSSEL,800,80));
        return cells;
    }

    public int getActieveSpelerID() {
        return actieveSpelerID;
    }

    public void setActieveSpelerID(int actieveSpelerID) {
        this.actieveSpelerID = actieveSpelerID;
    }

    public int getAantalSpelers() {
        return aantalSpelers;
    }

    public void setAantalSpelers(int aantalSpelers) {
        this.aantalSpelers = aantalSpelers;
    }

    public int getAantalVakken() {
        return AANTALVAKKEN;
    }

    public List<Cell> getVakken() {
        return vakken;
    }

    public void setVakken(ObservableList<Cell> vakken) {
        this.vakken = vakken;
    }

}
